require("./model/db connection")//db connection
const Express = require("./node_modules/express")//importing express 
const app = Express();//creating server
const config = require("./appconfig/config")//importing config
const bodyParser = require('body-parser')
const logger = require('morgan');
const userroutes=require("./routes/userroute")//importing userroutes
app.use(bodyParser.urlencoded({extended:false}))// Parses urlencoded bodies
app.use(bodyParser.json({extended:false}))  
app.use("/",userroutes)//middleware 
app.use(logger('dev')); // Log requests to API using morgan
app.listen(config.Port,config.Host,()=>{
 console.log("happy start");});//server is listening


 