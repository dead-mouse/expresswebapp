const Mongoose =require("../node_modules/mongoose");//imorting mongoose
const bcrypt = require('bcrypt');
var config = require("../appconfig/config")
var jwt = require('jsonwebtoken');
var User = Mongoose.model("user");//usermodelobject
module.exports.tokenValidator =(req,res,next)=>{
    var token = req.headers['token-to-auth'];
    console.log(token);
    if(!token){
        var error ={
            status:false,
            name:"Un Authorize",
            message:"Token Not Found",
            token:null
        };
        res.status(401).json(error);  
    }else{
        jwt.verify(token,config.secret,(tokenErr,result)=>{
            if(tokenErr){
                var error ={
                    status:false,
                    name:"Token Validaor Error",
                    message:"Invalid Token"
                };
                res.status(500).json(error);  
            }else{
                User.findById(result._id,(err,user)=>{
                    if(err){
                        var error ={
                            status:false,
                            name:"Internal Server Error",
                            message:"Failed To Authenticate"
                        };
                        res.status(500).json(error);  
                    }else if(!user){
                       var error ={
                           status:false,
                           name:"Un Autheriozed",
                           message:"Failed To Authenticate User"
                       };
                       res.status(401).json(error);  
                   }else{
                    // res.status(200).json(user);
                    next();
                   }
                });
            }
        });
        
        
    }
    
     
}
module.exports.registration=(request,responce)=>{ //user registration 
    var salt = bcrypt.genSaltSync(config.saltRounds);
      if(request.body.userName&&request.body.email&&request.body.password&&request.body.role){
        var hash = bcrypt.hashSync(request.body.password,salt)
        var user = new User({
            userName:request.body.userName,
            email:request.body.email,
            password:hash,
            role:request.body.role,
             });
      user.save((error,userdoc)=>{
          if(error){
              responce
              .status(404)
              .send({error:error,
                  message:"data insertion failed due to"})
          }else{
            var token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 86400,
                 // expires in 24 hours
              });
              responce
              .status(200)
              .send({//userdoc:userdoc,
                  token:token,
                  message:"data insertion successfully"}) 
          }
      })
    }
    else{
        responce
        .status(500)
        .send({
            message:"required feilds are missing"
        })
    }
    
}


module.exports.login = (request,responce)=>{ // user login
    var userName=request.body.userName
    if(request.body.userName&&request.body.password){
        User.findOne({userName},(error,userdoc)=>{
            if(error){
                responce
                .status(404)
                .json({
                    msg:"record not found",
                    error:error
                      })
                     }
            if(!userdoc){
                responce
                .status(500)
                .json({
                    msg:"user not found register immediately",
                    error:userdoc
                      })

            }else {
                var isvalid = bcrypt.compareSync(request.body.password,userdoc.password)
                if(isvalid){
                    responce
                    .status(200)
                   .json({
                    msg:"user found",
                    //userdoc:userdoc,
                    token:token,
                      })
                }else{
                    responce
                    .status(500)
                   .json({
                    msg:"password missmatch",
                    error:error
                })
            }}
        })
    }else
        {
            responce
            .status(404)
            .json({
                message:"required feilds are missing"
                  })

        }
    }