const Mongoose =require("../node_modules/mongoose");
var User = Mongoose.model("user");
module.exports.getUser =(request,responce)=>{
    var id = request.params.id;
    User.findById(id).exec((error,user)=>{
      if(error){
        responce.status(500);
        responce.json({
          Message:"Record not found",
        });
      }else{
        responce.status(200);
        responce.json({
          Message:"Records fetched successfully",
          responce:user
        });
      };  
    });
  };

  module.exports.createUser =(request,responce)=>{
    
    var user = new User({
      userName:request.body.userName,
       email:request.body.email,
       password:request.body.password,
      
       });
       user.save((error,user)=>{
      if(error){
        responce.status(500);
        responce.json({
          Message:"Record insertion failed",
        });
      }else{
        responce.status(200);
        responce.json({
          Message:"Record inserted successfully",
          responce:user
        });
      };  
    });
  };