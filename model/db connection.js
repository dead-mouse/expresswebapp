const mongoose = require('mongoose');// importing mongoose

const config = require("../appconfig/config")//importing config
require("./usermodel")
mongoose.connect(config.dbUrl,{ useNewUrlParser: true })//db connection object

const connection= mongoose.connection//returns connection object

connection.once("open",()=>{                    //"once" handler check weather db connection
    console.log("db connection sucessful");
    
});
connection.on("error",(error)=>{                // "on" handler always checks for error

    console.log("db connection failure",error);
    
});

