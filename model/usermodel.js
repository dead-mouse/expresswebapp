const Mongoose = require("mongoose");//importing mongoose odm tool
const Schema = Mongoose.Schema //calling schema
var userSchema = new Schema({  // creating user schema
    userName : {
       type:String,
      required:true,
       min:6,
       max:15},
    
    password  :
    {
      type:String,
      required:true,
      min:6,
      max:15
    },
    email: {
      type:String,
      unique:true
    },
    
    role:{
      type:String,
      default:'user'
    }
   
  
  })

Mongoose.model('user',userSchema,'userscollection');