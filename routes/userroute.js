const Express = require("express");//importing express 
const Router = Express.Router();
const userRoutes=require("../controllers/users")
const authCtrl=require("../controllers/authctrl")
Router.route('/user/:id') //get single user routes
.get(userRoutes.getUser)
Router.route('/user/')   // creating user routes
.post(userRoutes.createUser)
Router.route('/registration') // admin /user registration routes
.post(authCtrl.registration);
Router.route('/login')// admin /user login routes
.get(authCtrl.login);
router.route('/validate')
.get(authCtrl.tokenValidator);

module.exports=Router